# Rawscripts - Screenwriting Software
# Copyright (C) Ritchie Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, request, g, make_response, render_template_string
from rawscripts import app, connection
from rawscripts.db.models import db
from rawscripts.utils import permission
import config
import json
from datetime import datetime

#
# Any editing window (titlepage, screenplay, calendar) calls this
# function for template and data. This grabs the correct module for
# the item to be edited.
@app.route('/editor/<project_id>/<resource_id>', methods=['GET', 'POST'])
def editor(project_id, resource_id):
    #find which type of resource this is
    project = db.Projects.find_one({u'project_id': project_id})
    editor_type = ''
    for resource in project.resources:
        if resource['resource_id'] == resource_id:
            editor_type = resource['type']

    if editor_type == '': editor_type = request.values['type']

    # load module for that type
    name = 'rawscripts.resources.' + editor_type + '.' + editor_type
    resource_module = __import__(name, fromlist=['*'])
    reload(resource_module)

    if request.method == 'GET':
        # for GET request, get templates, load vairables, and spit it out

        # call for template name and template variables
        template = resource_module.get_template()
        template_values = resource_module.get_template_values(project_id, resource_id, config.MODE)

        return render_template_string(template, t=template_values)

    # For post requests, figure out the action to be taken
    r = request.values
    a = r['action']
    if a == 'get_current_save':
        #  get most recent save data
        response_text = resource_module.get_current_save_data(project_id, resource_id)

    elif a == 'save':
        # if action is 'save', save the incoming data

        #Create object for new save entry
        entry = db.ComponentData()
        entry.resource_id = resource_id

        # let module process save data if it wants
        data = resource_module.process_save_data(r['data'])
        entry.data = data

        # set correct autosave
        if r['autosave'] and r['autosave'] == '1':
            autosave = True
        else:
            autosave = False
        entry.autosave = autosave

        #get latest version number
        lastEntry = db.ComponentData.find_one({u'resource_id': resource_id}, sort=[(u'version', -1)])
        new_version = lastEntry.version + 1
        entry.version = int(new_version)

        #SAVE
        entry.save()

        # Update Project
        project.last_updated = datetime.utcnow()
        for i in project.resources:
            if i['resource_id'] == resource_id:
                i['last_updated'] = datetime.utcnow()
                break
        project.save()

        response_text = '1'
    elif a == 'rename':
        project.title = r['new_title']
        for resource in project.resources:
            if resource['resource_id'] == resource_id:
                resource['title'] = r['new_title']
                break

        project.save()
        response_text = r['new_title']

    elif a == 'create_new_resource':
        resource_id = resource_module.create_new_resource(project_id)
        response_text = resource_id

    # Voila. With the response text, create a full http response, and
    # send it out.
    response = make_response(response_text)
    response.headers["Content-type"] = "text/plain"
    return response
