# Rawscripts - Screenwriting Software
# Copyright (C) Ritchie Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, request, render_template, g, make_response
from rawscripts import app
from rawscripts.db.models import db
from rawscripts.utils import permission
import config
import json
from collections import deque
import sys
import string
import time
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.platypus import Paragraph 
from reportlab.lib.styles import getSampleStyleSheet 
from reportlab.rl_config import defaultPageSize
import reportlab
import StringIO

@app.route('/export')
def export():
    r = request.values
    r_id=r['resource_id']
    p = permission(r_id)
    if p!='owner':
            return 'Permission error'
    export_format = r['format']
    title_page = r['titlepage']
    title = db.Projects.find_one({'resources.resource_id' : r_id}).title
    screenplay = db.ComponentData.find_one({u'resource_id' : r_id}, sort = [(u'version', -1)])
    if export_format =='txt':
	    o_file = Text(screenplay.data,title, title_page, r_id)
	    header = 'text/plain'
	    extension = '.txt'
    elif export_format == 'pdf':
	    o_file = Pdf(screenplay.data,title, title_page, r_id)
	    header = 'application/pdf'
	    extension = '.pdf'
    response = make_response(o_file.getvalue())
    response.headers['Content-type'] = header
    response.headers['Content-Disposition'] = 'attachment; filename='+title+extension
    return response

def Text(data, title, title_page, resource_id):
	widths=[[62,15,1],[62,15,1],[40,35,0],[35,25,1],[35,30,0],[62,61,1]]
	txt = json.loads(data)
	
	s = StringIO.StringIO()
	
	s.write('\n\n\n')
	parenTest=False
	for i in txt:
			#lingering parentheses problem
			if parenTest==True:
					if not int(i[1])==4:
							s.write('\n')
					parenTest=False
			
			words = deque(i[0].split(' '))
			if not int(i[1])==5:
					spaces=widths[int(i[1])][1]
			else:
					diff=0
					for j in words:
							diff+=len(j)+1
					spaces=77-diff
			k=0
			while k<spaces:
					s.write(' ')
					k+=1
					
			linewidth=0
			
			for j in words:
					if linewidth+len(j)>widths[int(i[1])][0]:
							linewidth=0
							s.write('\n')
							k=0
							while k<widths[int(i[1])][1]:
									s.write(' ')
									k+=1
					if int(i[1])==0:
							v=j.upper()
					elif int(i[1])==2:
							v=j.upper()
					elif int(i[1])==5:
							v=j.upper()
					else:
							v=j
					s.write(v)
					s.write(' ')
					linewidth+=len(j)+1
			s.write('\n')
			#save paren for next time around to be sure
			if int(i[1])==3:
					parenTest=True
			elif widths[int(i[1])][2]==1:
					s.write('\n')
		
	return s

def Pdf(data, title, title_page, resource_id):
	buffer = StringIO.StringIO()
	c = canvas.Canvas(buffer)
	c.setFont('Courier',11)
	lh=12
	
	if str(title_page)=='true':
            title_page = False
            project = db.Projects.find_one({'resources.resource_id' : resource_id})
            for r in project['resources']:
                if r['type']=='titlepage':
                    title_page = db.ComponentData.find_one({u'resource_id' : r['resource_id']}, sort = [(u'version', -1)])

            if title_page:
                titlepage_fields = json.loads(title_page.data)
		ty = 550 #title y
		tx = defaultPageSize[0]/2.0 #title x (center)
		ly = 200 #left box coordinat
		lx = 120 #right box coordinate
                ry = ly #right box corrdinate
                rx = 380 #right box coordinate
		style = getSampleStyleSheet()["Normal"]
		style.fontName = 'Courier'
		style.fontSize = 11
                title_lines = titlepage_fields['title'].split('<br>') # written by lines of text
                for i in title_lines:
                    p = Paragraph("<para alignment='center'><u>"+i+"</u></para>", style) 
                    w,h = p.wrap(600, 100)
                    p.drawOn(c, tx-(w/2.0), ty)
                    ty-=lh
		ty-=lh*2
                wb_lines = titlepage_fields['written_by'].split('<br>') # written by lines of text
                for i in wb_lines:
                    c.drawCentredString(tx, ty, i)
                    ty-=lh

                le_lines = titlepage_fields['left_editable'].split('<br>') # left editable lines of text
                for i in le_lines:
                    c.drawString(lx, ly, i)
                    ly-=lh

                re_lines = titlepage_fields['right_editable'].split('<br>') # right editable lines of text
                for i in re_lines:
                    c.drawString(rx, ry, i)
                    ry-=lh

		c.showPage()
		
	# Calc wrapping text
	# end up with an array linesNLB
	# just like the var in js
	widths=[62,62,40,36,30,62]
	b_space=[1,1,0,1,0,1]
	printX=[100,100,232,166,199,503]
	lines = json.loads(data)
	
	j=0 ## dumb iterator. used to handle mutiple parentheticals in one dialog
	lc='' # keep track of recenct character to speak for CONT'D
	linesNLB=[]
	for i in lines:
		wa=i[0].split(' ')
		phraseArray=[]
		lastPhrase=''
		l=widths[int(i[1])]
		measure=0
		itr=0
		
		# test if should be uppercase
		if i[1]==0 or i[1]==2 or i[1]==5:
			uc=True
		else:
			uc=False
		for w in wa:
			itr+=1
			measure=len(lastPhrase+" "+w)
			if measure<l:
				lastPhrase+=w+" "
			else:
				if uc:
					lastPhrase=lastPhrase.upper()
					if i[1]==2 and lastPhrase==lc+' ':
						lastPhrase+="(CONT'D) "
				phraseArray.append(lastPhrase[0:-1])
				lastPhrase=w+' '
			if itr==len(wa):
				if uc:
					lastPhrase=lastPhrase.upper()
					if i[1]==2 and lastPhrase==lc+' ':
						lastPhrase+="(CONT'D) "
				phraseArray.append(lastPhrase[0:-1])
				break
		itr=0
		while itr<b_space[int(i[1])]:
			phraseArray.append('')
			itr+=1
		if i[1]==4 and j!=0 and lines[j-1][1]==3:
			linesNLB[j-1].pop()
		linesNLB.append(phraseArray)
		j+=1
		if i[1]==2:
			lc=i[0].upper()
		elif i[1]==0:
			lc=''
	
	#pagination, as done in
	# editor.js
	pageBreaks = []
	i=0
	r=0
	trip=False
	while i<len(lines):
		lineCount = r
		while lineCount+len(linesNLB[i])<56:
			lineCount+=len(linesNLB[i])
			i+=1
			if i==len(lines):
				trip=True
				break
		if trip==True:
			break
		
		s=0
		r=0
		if lines[i][1]==3 and lineCount<54 and lineCount+len(linesNLB[i])>57:
			s=55-lineCount
			r=1-s
			lineCount=56
		elif lines[i][1]==3 and lineCount<54 and len(linesNLB[i])>4:
			s=len(linesNLB[i])-3
			r=1-s
			lineCount=55
		elif lines[i][1]==1 and lineCount<55 and lineCount+len(linesNLB[i])>57:
			s=55-lineCount
			r=1-s
			lineCount=56
		elif lines[i][1]==1 and lineCount<55 and len(linesNLB[i])>4:
			s=len(linesNLB[i])-3
			r=1-s
			lineCount=55
		else:
			while lines[i-1][1]==0 or lines[i-1][1]==2 or lines[i-1][1]==4:
				i-=1
				lineCount-=len(linesNLB[i])
		
		pageBreaks.append([i, lineCount, s])
	
	
	
	
	# draw text onto pdf, just like
	# it's done in editor.js
	pageStartY = 740
	y=pageStartY
	numX=483
	numY= pageStartY+(lh*3)
	count=0
	latestCharacter=''
	c.setFont('Courier',11)
	for i in range(0,len(linesNLB)):
		if lines[i][1]==2:
			latestCharacter=lines[i][0]
		for j in range(0,len(linesNLB[i])):
			if len(pageBreaks)!=0 and pageBreaks[count][0]==i and pageBreaks[count][2]==j:
				if j!=0 and lines[i][1]==3:
					c.drawString(printX[2], y, "(MORE)")
				if count!=0:
					c.drawRightString(numX,numY, str(count+1)+'.')
				c.showPage()
				c.setFont('Courier',11)
				y=pageStartY
				count+=1
				if j!=0 and lines[i][1]==3:
					c.drawString(printX[2], y, latestCharacter.upper()+" (CONT'D)")
					y-=lh
				if count>=len(pageBreaks):
					count=len(pageBreaks)-2
			if lines[i][1]==5:
				c.drawRightString(printX[int(lines[i][1])], y, linesNLB[i][j])
			else:
				c.drawString(printX[int(lines[i][1])], y, linesNLB[i][j])
			y-=lh
	
	#close last page
	if len(pageBreaks)!=0:
		c.drawRightString(numX,numY, str(len(pageBreaks)+1)+'.')
	c.showPage()
	c.save()

	return buffer
