from flask import Flask, request, render_template, g, make_response, session, redirect, url_for
from werkzeug import secure_filename
from rawscripts import app, connection
from rawscripts.db.models import db
from rawscripts.utils import create_project, create_resource
import config
import convert

import StringIO
import json
from datetime import datetime

ALLOWED_EXTENSIONS = set(['txt', 'celtx', 'fdx'])

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[-1] in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'GET':
        # this is an iframe in the scriptlist page that lets people
        # upload screenplays to be converted
        g.t['URL'] = config.URL
        return render_template('upload_form.html', t=g.t)
    else:
        #handling the post data when a screenplay is uploaded

        # make some effort to make sure good file was uploaded
        file = request.files['screenplay']
        if not (file and allowed_file(file.filename)):
            return render_template('upload_form.html', t=g.t)

        # get correct title
        title = secure_filename(file.filename)
        title_parts = title.split('.')
        if title_parts[-1]=='':
            title_parts.pop()
        file_extention = title_parts.pop()
        title = unicode('.'.join(title_parts))
        
        # create new project and resource
        project_id = create_project(title)
        resource_id = create_resource(project_id, 'screenplay')

        # Convert screenplay to rawscripts format
        data = file
        if file_extention == 'txt':
            contents = convert.Text(data)
        elif file_extention == 'fdx':
            contents = convert.FinalDraft(data)
        else:
            contents = convert.Celtx(data)
        contents = unicode(contents)

        # create first save
        d = db.ComponentData()
        d.resource_id = unicode(resource_id)
        d.version = 1
        d.autosave = False
        d.data = contents
        d.save()

        template_values={
            'project_id': project_id,
            'resource_id': resource_id,
            'url': config.URL,
            }
        return render_template('upload_complete.html', t=template_values)
