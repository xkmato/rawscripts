.. _working-on-code:

==========================
 Working On Code
==========================

.. contents:: Sections
   :local:

These instructions are for Mac and Linux users. We welcome Windows
users, but I havn't the foggiest which of this applies to you.

Download With Git
===================

Download the code from https://gitorious.org/rawscripts/rawscripts

--OR--

Use git::

    git clone git://gitorious.org/rawscripts/rawscripts.git

At the moment, RawScripts runs on Google AppEngine, but all my work
right now is put towards getting it off of AppEngine. So right now,
the branch "master" contains all the AppEngine specific code. The
branch "freedom" contains all the new code for getting off of
AppEngine. Please Work From The Freedom Branch.

First see where the "freedom" branch is::
    git branch -a

The first time, the "freedom" branch will likely be listed as
something like "remotes/origin/freedom". To make it a local, workable
branch, use ``git checkout -t remote/origin/freedom``. Otherwise, just
use ``git checkout freedom``.

Once you have checked out ``freedom``, create your own branch to work
in::

    git branch awesome_feature
    git checkout awesome_feature

Then start working on your awesome feature.

Setup Your Environment
======================

The best way to handle working with RawScripts is to set up
`virtualenv <http://www.virtualenv.org/>`_. This allows you to use
various python packages by just installing them to the rawscripts
directory. Get virtualenv installed, then from the command line,
change directories into the rawscripts project. Make that directory a
virtualenv with::

    virtualenv ./

This creates a whole python file structure in that directory. Then
activate virtualenv with::

    source bin/activate

Now you can add install python packages for Rawscripts while keeping
them all within the Rawscripts project. Neat!


Get the Libraries Used
======================

There is a new script to help grab all the needed libraries. From the
project's root directory run::

    python bootstrap.py

That grabs for you all the needed python packages described in
requirments.txt. It also downloads or updates the Google Closure
Library (Javascript and CSS).


Running Locally
===============

There is a sample config.py.sample, and you should be able to copy
that to config.py. The settings in that file should be
obvious. They're all set up for development, so if you don't know, you
probabaly don't need to touch 'em.

Start up Mongo!

If all the previous steps have gone well, then from the root of the
project run ``python runserver.py``. Open a browser, and go to
`http://localhost:5000 <http://localhost:5000>`_.
