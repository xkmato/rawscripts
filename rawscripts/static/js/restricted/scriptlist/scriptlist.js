/**
 * Rawscripts - Screenwriting Software
 * Copyright (C) Ritchie Wilson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function RawScripts(){
    this.folders = {};
    this.folders.open_folder = 'ownedFolder';
}

var rs = new RawScripts();


$(document).ready(function(){
    $('button').button();
    
    // set correct element sizes though jquery, not css
    $(window).resize(function(){
	set_element_sizes();
    })
    set_element_sizes();
    $('#remove_projects_from_trash').css({'display':'none'});
    $('#ownedFolder').click(function(){
	$('.folder-current').removeClass('folder-current');
	$(this).addClass('folder-current');
	$('#remove_projects_from_trash').css({'display':'none'});
	rs.folders.open_folder = 'ownedFolder';
	show_projects_in_folder();
    })
    $('#trashFolder').click(function(){
	$('.folder-current').removeClass('folder-current');
	$(this).addClass('folder-current');
	$('#remove_projects_from_trash').css({'display':'inline-block'});
	rs.folders.open_folder = 'trashFolder';
	show_projects_in_folder();
    })

    
    
    // add droppable action to trash folder
    $('#trashFolder').droppable({
	hoverClass: 'ui-state-hover',
	drop: function(event,ui){
	    var project_id = ui.draggable.attr('data-project_id');
	    for(i in projects){
		if (projects[i].project_id==project_id){
		    var project = projects[i];
		}
	    }
	    project.move_to_trash();
	}
    });
    
    refreshList();
})

/**
 * Set correct sizes for elements that cannot be properly set with
 * css.
 */
function set_element_sizes(){
    var list_height = $(window).height()-$('#header').height()-$('#scriptlist_header').height()-2;
    $('#list').height(list_height+'px')
}

var projects = [];

/**
 * Method on Project, takes it's data and creates an HTML entry in the
 * scriptlist.
 */
Project.prototype.createHTML = function(){
    var project_id = this.project_id;
    
    // create container div
    this.container = $(document.createElement('div'));
    this.container.attr({
	'class':'entry',
	'data-project_id':project_id,
	'data-resource_id':this.resource_id,
	'id': this.project_id
    })
    
    //create table
    var table = $(document.createElement('table')).attr({'width':'100%'})
    this.container.append(table)

    // create row in table
    var row = $(document.createElement('tr'))
    table.append(row)
    
    // create grab handle
    this.handle = $('<td>').attr({
	'class':'handleCell'
    });
    var title = this.title;
    row.append(this.handle);
    this.container.draggable({
	revert:'invalid',
	handle:this.handle,
	helper: function(){
	    return $( "<div class='ui-widget-header'>"+title+"</div>" );
	}
    });
    
    // create columns						  
    // first the checkbox
    this.checkbox = $(document.createElement('input')).attr({
	'type':'checkbox',
	'name':'listItems',
	'value':project_id
    });
    $(this.checkbox).click(function(){check_select_all_status()})
    // put the checkbox in a cell in the row
    row.append($(document.createElement('td')).attr({'class':'checkboxCell'}).append(this.checkbox));

    //then create the title spot
    this.title_cell = $(document.createElement('td')).attr({'class':'title_cell'});
    this.title_cell.html("<span class='title_span'>"+this.title+'</span>')
    $(this.title_cell).click(function(){
	display_resources(project_id)
    })
    row.append(this.title_cell);
    
    // create folder column
    this.folder_cell = $(document.createElement('td')).attr({
	'align':'center',
	'class':'folderCell'
    });
    row.append(this.folder_cell);
    
    // create action box. All minor actions accesable by hover are put
    // in this box
    this.action_cell = $(document.createElement('td')).attr({
	'align':'center',
	'class':'actionCell'
    });
    row.append(this.action_cell);
    this.action_box = $('<div>').attr({'class':'action_box'});
    this.action_cell.append(this.action_box);
    
    // create actions on screenplay
    var email = {
	'title':'Email',
	'action':'email',
	'icon':'email.svg'
    }
    var share = {	
    	'title':'Share',
	'action':'share',
	'icon':'share.svg'
    }
    this.actions = [share, email];
    var project = this;
    for (i in this.actions){
	var action = this.actions[i];
	this.action_box.append(
	    $('<img>').attr({
		'src':'/static/images/'+action.icon,
		'class':'action',
		'title': action.title
	    }).hover(
		function(){$(this).animate({'opacity':1})},
		function(){$(this).animate({'opacity':0.5})}
	    ).click(function(){
		if(action.action=='email'){
		    project.email_prompt();
		}
	    })
	)
    }
    
    var entry = this;
    this.container.hover(
	function(){entry.action_box.fadeIn()},
	function(){entry.action_box.hide()}
    )
    // last updated column
    this.last_updated_cell = $(document.createElement('td')).attr({
	'align':'center',
	'class':'updatedCell'
    });
    var updated = last_updated_to_string(this.last_updated);
    this.last_updated_cell.html(updated);
    row.append(this.last_updated_cell);
    

    return this.container
}

/**
 * Method to entirely remove the project element in the list
 */
Project.prototype.destroyHTML = function(){
    this.container.remove()
}

Project.prototype.hideHTML = function(){
    this.container.css({'display':'none'})
}

Project.prototype.showHTML = function(){
    this.container.css({'display':'block'})
}

Project.prototype.add_project_to_folder = function(folder){
    // first make sure this project isn't already in that folder
    if(jQuery.inArray(folder.id, this.folders)>=0)return;
    
    // collect varialbes and send them to server
    var project = this;
    $.post('/projects',
	   { action:'add_project_to_folder',
	     id: folder.id,
	     project_id:project.project_id,
	   },
	   function(data){
	       if(data.id==undefined)return ERROR('hey, whatnow?')
	       
	       // add project_id to correct folder object
	       for (i in folders){
		   if (folders[i].id==data.id){
		       folders[i].projects.push(project.project_id);
		   }
	       }
	       
	       //redraw
	       project.create_folders_in_project();

	   },
	   'json'
	  ).error(function(){ERROR('Server Error, Holmes')});
}

/**
 * Method to remove a project from a folder.
 */
Project.prototype.remove_project_from_folder = function(folder_id){
    // we got a folder id and project, send shit to the server
    var project = this;
    $.post('/projects',
	   { action:'remove_project_from_folder',
	     project_id:project.project_id,
	     folder_id:folder_id,
	   },
	   function(data){
	       // check we got back some useful data
	       if (data.folder_id==undefined)return ERROR('Didnt get back what we expected');
	       
	       // add project to the correct folder
	       for (i in folders){
		   if(folders[i].id==data.folder_id){
		       var pos = jQuery.inArray(data.project_id, folders[i].projects); // position of this project in array
		       folders[i].projects.splice(pos, 1);
		   }
	       }
	       
	       // redraw html for folders in the project list
	       project.create_folders_in_project();
	   },
	   'json'
	  ).error(function(){ERROR("Couldn't remove project from folder")})
}

/**
 * For a project, look through which folders it is in, then create
 * icons and options for that in the entry div
 */
Project.prototype.create_folders_in_project = function(){
    // clear out this project's folder data, then repopulate it
    this.folders=[];
    for (i in folders){
	if (jQuery.inArray(this.project_id, folders[i].projects)>=0)
	    this.folders.push(folders[i].id)
    }
    
    //quickly clear out folder span so everything can be rebuilt
    this.folder_cell.html('');
    
    // project can be in multiple folders, so cycle through each,
    // creating new elements
    for (i in this.folders){
	// create a containter div to hold name and button
	var folder_div = $('<div>')
	    .attr({'class':'folder-in-project'});
	this.folder_cell.append(folder_div);
	
	//figure out name of folder
	var name = '';
	for(j in folders){
	    if(this.folders[i]==folders[j].id)name=folders[j].name;
	}
	
	// Add a div with the folder name
	var name_div = $('<div>')
	    .attr({'class':'folder-in-project-name'})
	    .html(name);
	folder_div.append(name_div);
	
	
	// create and add a div 
	var button_div = $('<div>')
	    .attr({'class':'folder-in-project-button ui-icon ui-icon-close'})

	// set data on button div to be recalled when clicked
	button_div.data('folder_id', this.folders[i]);

	// add an onclick method to the folder div
	var project = this;
	button_div.click(function(){
	    project.remove_project_from_folder($(this).data('folder_id'));
	});

	folder_div.append(button_div);
    }
    
    // Finally add this to the correct static folder (trash, owned,
    // shared)
    if(this.is_trashed){
	this.folders.push('trashFolder');
    }
    else{
	this.folders.push('ownedFolder');
    }
}

/*
 * When it is posible to create more usefull resources as part of a
 * project, this function will have a GUI that shows, say, Screenplay,
 * Calendar, StoryBoard, etc. A user could then open one
 * individually. Right now, it just opens the correct screenplay.
 */
function display_resources(project_id){
    for(i in projects){
	if (projects[i].project_id==project_id){
	    var url = 'editor/'+project_id+'/'+projects[i].resource_id;
	    window.open(url);
	}
    }
}


/**
 * Calls the server for all screenplay information including names,
 * last modified, folders, permissions, unread notes, unread shared
 * scripts, etc. Server responds with JSON, function cleans out old
 * data, then puts new data in it's place
 */
function refreshList(){
    $('#refresh_icon').css({'visiblity':'visible'});
    $.post('projects',
	   {action:'projects_list'},
	   function(data){
	       
	       // CREATE PROJECT OBJECTS
	       var x=data.projects;
	       $('#loading').css({'display':'none'});
	       
	       //remove old entries
	       for(i in projects){
		   projects[i].destroyHTML()
	       }
	       projects = [];
	       
	       var display = (x.length==0 ? "block" : "none");
	       $('#noentries').css({'display':display});
	       
	       for (var i=0; i<x.length; i++){
		   var project = new Project();
		   project.title = x[i]['title'];
		   project.project_id = x[i]['project_id'];
		   project.resource_id = x[i]['resources'][0]['resource_id'];
		   project.last_updated = new Date(x[i]['last_modified']);
		   project.is_trashed = x[i]['is_trashed'];
		   var shared_with=[];
		   var new_notes=[];
		   projects.push(project);
		   var entry_div = project.createHTML();
		   $('#list').append(entry_div);
	       }
	       
	       // CREATE FOLDER OBJECTS
	       for (i in folders){
		   folders[i].destroyHTML();
	       }
	       folders=[];
	       
	       var f = data.folders;
	       for (i in f){
		   var folder = new Folder(f[i].id,f[i].name, f[i].projects);
		   folders.push(folder);
		   var folder_div = folder.createHTML();
		   $('#user_folders').append(folder_div);
	       }
	       
	       // add folder spans to project divs
	       for (i in projects){
		   projects[i].create_folders_in_project();
	       }
	       
	       show_projects_in_folder();
	       // hide the reresh icon. we're done!
	       $('#refresh_icon').css({'visibility':'hidden'})
	       set_element_sizes();
	   },
	   'json'
	  );
}

/**
 * On check of select all checkbox, select or deselect all the checkboxes
 * @ params {html element} obj the input checkbox 
 */
function select_all(obj){
    var bool = obj.checked
    for(i in projects){
	projects[i].checkbox.prop('checked', bool)
    };
}

/**
 * called when a user checks a box. If all projects are selected,
 * check the select_all box. If at least one is not checked, uncheck
 * the select_all checkbox.
 */
function check_select_all_status(){
    $('#select_all').prop('checked', true);
    for(i in projects){
	if(!projects[i].checkbox.is(':checked')){
	    $('#select_all').prop('checked', false);
	    return;
	}
    }
}



/**
 * Opens the upload prompt on click
 */
function upload_prompt(){
    var d = $('<div>');
    d.append($('<h2>').html('Upload Screenplay'));
    
    // Upload is done in an iframe, so it can be uploaded without
    // visiting another page.
    d.append(
	$('<iframe>').attr({
	    'id':'uploadFrame',
	    'frameborder':'0',
	    'height':'170px',
	    'width': '350px',
	    'src':'/upload'
	})
    )
    // Turn that html into the jquery dialog box
    d.dialog({
	modal:true,
	resizable:false,
	width:'400px',
	close:function(){d.remove()}
    });
}

/**
 * Add listener for messages from upload iframes
 */
window.addEventListener("message", recieveMessage, false);

/**
 * Takes that message from iframe and shows correct
 * GUI, either "Loading" bar, or "Complete" message
 * @ params {event object} e Contains data from cross
 * iframe message event
 */
function recieveMessage(e){
	//if(e.origin!="http://www.rawscripts.com")return;
	if(e.data=="uploading"){
	    // TODO 
	    // update GUI in jQuery
	}
	else{
	    window.open("/editor/"+e.data);
	    //TODO close the dialog box after some time
	    refreshList();
	}
    
}

/**
 * Update the DOM after renaming project
 */
function rename_project_return(project){
    project.title_cell.html('<span>'+project.title+'</span>');
    
}

/**
 * Opens the rename prompt on click
 */
function renamePrompt(){
    // first check that user has selected only
    // one script to rename
    var project = false;
    for (i in projects){
	if (projects[i].checkbox.is(':checked')){
	    if(project!=false){
		alert("Please select one at a time");
		return;
	    }
	    project = projects[i];	    
	}
    }
    // if only one project is selected, open rename prompt for that project
    project.rename_prompt();
}




/**
 * Takes checked boxes (selected scripts), then creates a table in the
 * export prompt giving options for title page and export format.
 */
function export_prompt(){
    // check to make sure at least one project is selected
    var project_found = false;
    for (i in projects){
	if(projects[i].checkbox.is(':checked')){
	    project_found=true;
	    break;
	};
    };
    if(!project_found)return ERROR('Please select a project to export');
    
    // Create html that will be turned into dialog box
    var d = $(document.createElement('div'));
    this.dialog_html = d;
    d.append($(document.createElement('h2')).html('Export'));
    var table = $(document.createElement('table'));
    table.append($('<tr>')
		 .append($('<th>').html('Title').css({'width':'33%'}))
		 .append($('<th>').html('Format').css({'width':'33%'}))
		 .append($("<th>").html('Titlepage').css({'width':'33%'}))
		)
    d.append(table);
    
    // cycle through projects. For each that is checked, create a row
    // in the table
    for(i in projects){
	if(projects[i].checkbox.is(':checked')){
	    // add export options to dict, then add that dict to the
	    // project
	    var export_options = {};
	    
	    // create the row that goes in a table
	    var tr = $('<tr>');
	    table.append(tr);
	    
	    // title cell, add it to row
	    var td = $('<td>').css({'width':'33%'});
	    td.html(projects[i].title);
	    tr.append(td);
	    
	    // format cell, add it to row
	    var td = $('<td>').css({'width':'33%', 'text-align':'center'});
	    export_options.format = $('<select>');
	    var option = $('<option>').val('pdf').html('PDF');
	    export_options.format.append(option);
	    var option = $('<option>').val('txt').html('.txt');
	    export_options.format.append(option);
	    tr.append(td.append(export_options.format));
	    
	    // add titlepage options
	    var td = $('<td>').css({'width':'33%', 'text-align':'center'});
	    export_options.titlepage = $("<input type='checkbox'>");
	    tr.append(td.append(export_options.titlepage));
	    
	    // add this row to the table
	    table.append(tr)
	    projects[i].export_options = export_options;
	};
    };
    $('body').append(d);
    var project = this;
    
    d.dialog({
	modal:true,
	resizable:false,
	width:'500px',
	buttons:[{
	    text:'Export',
	    click:function(){
		export_projects();
		d.dialog('close');
	    }
	}],
	close:function(){d.remove()}
    });
}

/**
 * Takes checked boxes (selected scripts), then creates a table in the
 * export prompt giving options for title page and export format.
 */
function export_projects(d){
    // cycle through projects tooking for ones to export
    for(i in projects){
	if(projects[i].checkbox.is(':checked')){
	    // assemble url
	    var p = projects[i];
	    var url = '/export?resource_id=';
	    url+= p.resource_id;
	    
	    // cycle through export options, adding info to GET url
	    for (option in p.export_options){
		url+='&'+option;
		if(p.export_options[option].is(':checkbox')){
		    url+='='+p.export_options[option].is(':checked');
		}
		else{
		    url+='='+p.export_options[option].val();
		}
	    }
            window.open(url);
	}
    }
}

function last_updated_to_string(last_modified){
    var months = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    var now = new Date;
    var time_zone_difference = now.getTimezoneOffset() * 60 * 1000 // get timedifference as miliseconds
    
    var diff = (now-last_modified+time_zone_difference)/1000; // how old a project is in seconds

    // If a project is older than two weeks, just show the date
    if (diff> 60 * 60 * 24 * 14){
	return months[last_modified.getMonth()]+' '+last_modified.getDate()
    }
    
    // for more than 48 hours, show how many days ago
    if (diff > 60 *60 * 24 *2){
	return Math.round(diff/(60 * 60 * 24)) + ' days ago';
    }
    // for one day, don't do it plural
    if (diff > 60 *60 * 24){
	return 'One day ago';
    }
    
    // for more than an hour
    if (diff > 60 *60){
	return Math.round(diff/(60 * 60)) + ' hours ago';
    }
    
    // for more than two minutes
    if (diff > 60 * 2){
	return Math.round(diff/60) + ' minutes ago';
    }
    
    // for just one minute
    if (diff > 60){
	return 'One minute ago';
    }
    
    // if this was just created
    return 'Seconds ago';
    
    
    return months[last_modified.getMonth()]+' '+last_modified.getDate()
    return 'test'
}


Project.prototype.move_to_trash = function(){
    var project = this;
    $.post('/projects',
	   {
	       action:'move_project_to_trash', 
	       project_id:project.project_id
	   },
	   function(data){
	       if(data.project_id==undefined)return ERROR('Didnt delete it. sorry');
	       project.is_trashed=true;
	       project.folders.push('trashFolder');
	       project.hideHTML();
	   },
	   'json'
	  )
}


function show_projects_in_folder(){
    var f = rs.folders.open_folder;
    for (i in projects){
	if(jQuery.inArray(f, projects[i].folders)>=0){
	    projects[i].showHTML();
	}
	else{
	    projects[i].hideHTML();
	}
	
	// make sure trashy things are only open for trash
	if(f!='trashFolder' && projects[i].is_trashed){
	    projects[i].hideHTML();
	}
    }
}



/**
 * onClick action attached to html button. Loops through projects and
 * finds apropirate ones to remove from trash
 */
function remove_projects_from_trash(){
    for(i in projects){
	if (projects[i].checkbox.is(':checked') && projects[i].is_trashed){
	    projects[i].remove_project_from_trash();
	}
    }
}

Project.prototype.remove_project_from_trash = function(){
    var project = this;
    $.post('/projects',
	   {action:'remove_project_from_trash', project_id: projects[i].project_id},
	   function(data){
	       project.is_trashed = false;
	       project.create_folders_in_project();
	       show_projects_in_folder();
	   },
	   'json'
	  )
}





function after_create_new_project(data){

    // if we do get the data we expect, create a new project object
    var p = new Project();
    p.title = title;
    p.resource_id = data.resource_id;
    p.project_id = data.project_id;
    p.folders = ['ownedFolder'];
    p.last_updated = new Date(data.last_updated);
    
    // update html by creating new entry in the list and closing dialog
    projects.push(p);
    $('#list').prepend(p.createHTML());
    
    var display = (projects.length==0 ? "block" : "none");
    $('#noentries').css({'display':display});
}