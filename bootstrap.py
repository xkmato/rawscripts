import os, subprocess

root = os.getcwd()

# get needed python packages from requiremtns.txt
subprocess.call(['pip','install','-r','requirements.txt'])

# get closure compiler if needed
c_dir = root+'/scripts/closure/'
if not os.path.exists(c_dir):
    os.makedirs(c_dir)
    print "made folders"

# get or update closure library
c_dir = root+'/rawscripts/static'
os.chdir(c_dir)
subprocess.call(['svn', 'checkout', 'http://closure-library.googlecode.com/svn/trunk/', 'closure-library'])