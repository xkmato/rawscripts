from flaskext.openid import OpenID
from rawscripts import app
from rawscripts.db.models import db
from flask import g, session, render_template, request, redirect, flash, url_for
from datetime import datetime
import config

# setup flask-openid
oid = OpenID(app)


@app.before_request
def get_current_user():
    g.user = None
    if 'openid' in session:
        g.user = db.Users.find_one({'openid' : session['openid']})

@app.route('/login', methods=['GET', 'POST'])
@oid.loginhandler
def login():
    """Does the login via OpenID.  Has to call into `oid.try_login`
    to start the OpenID machinery.
    """
    
    # allow for admin login in DEV mode
    if 'admin' in request.values:
        session['openid']='admin'
        return redirect(url_for('create_profile'))
    
    # if we are already logged in, go back to were we came from
    if g.user is not None:
        return redirect(url_for('projects'))
    if request.method == 'POST':
        openid = request.form.get('openid')
        if openid:
            return oid.try_login(openid, ask_for=['email', 'fullname',
                                                  'nickname'])
    t={'MODE': config.MODE, 'GA' : config.GA}
    return render_template('welcome.html', next=oid.get_next_url(),
                           error=oid.fetch_error(), t=t)

@oid.after_login
def create_or_login(resp):
    """This is called when login with OpenID succeeded and it's not
    necessary to figure out if this is the users's first login or not.
    This function has to redirect otherwise the user will be presented
    with a terrible URL which we certainly don't want.
    """
    session['openid'] = resp.identity_url
    user = db.Users.find_one({'openid' : resp.identity_url})
    if user is not None:
        flash(u'Successfully signed in')
        user.last_login = datetime.utcnow()
        user.save()
        g.user = user
        return redirect(url_for('projects'))
    return redirect(url_for('create_profile', next=oid.get_next_url(),
                            name=resp.fullname or resp.nickname,
                            email=resp.email))

@app.route('/create-profile', methods=['GET', 'POST'])
def create_profile():
    """If this is the user's first login, the create_or_login function
    will redirect here so that the user can set up his profile.
    """
    if g.user is not None or 'openid' not in session:
        return redirect(url_for('projects'))
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        if not name:
            flash(u'Error: you have to provide a name')
        elif '@' not in email:
            flash(u'Error: you have to enter a valid email address')
        else:
            flash(u'Profile successfully created')
            last_user = db.Users.find_one(sort=[(u'user_id', -1)])
            if last_user:
                user_id = last_user.user_id+1
            else:
                user_id = 1
            user = db.Users()
            user.user_id = user_id
            user.openid = unicode(session['openid'])
            user.email = email
            user.nickname = name
            user.save()
            return redirect(url_for('projects'))
    return render_template('create_profile.html', next_url=oid.get_next_url())


@app.route('/logout')
def logout():
    session.pop('openid', None)
    flash(u'You have been signed out')
    return redirect('/')

