var fade_timer = 2000;
var t // set global variable for setTimeout
var input_background_color
var input_border_color

$(document).ready(function(){
    
    var input = $('#written_by_editable');
    input_background_color = input.css('background-color');
    input_border_color = input.css('border-top-color');
    console.log(input_background_color)
    console.log(input_border_color)
    
    // first fade
    t = setTimeout('fade_out_inputs()', fade_timer)
    
    // on any mouse move, fade up input backgrounds
    $(document).mousemove(function(){
	$('.editable').stop();
	fade_up_inputs();
	clearTimeout(t);
	t = setTimeout('fade_out_inputs()', fade_timer)
    });

    // on any keydown, allow for new saving
    $('.editable').keyup(function(){
	var sb = $('#saveButton')
	sb.removeAttr('disabled');
	sb.attr('value', 'Save');
    })
    
})

function closeTitlePage(){
    window.close();
}


function save(){    
    var sb = $('#saveButton');
    sb.attr('disabled', 'disabled');
    sb.attr('value', 'Saving');

    fade_out_inputs();

    var title = $("#title_editable").html();
    var written_by = $("#written_by_editable").html();
    var left_editable = $("#left_editable").html();
    var right_editable = $("#right_editable").html();
    var data = {
	title:title,
	written_by:written_by,
	left_editable:left_editable,
	right_editable:right_editable
    }
    data = JSON.stringify(data)
    $.post("/editor/"+project_id+'/'+resource_id,
	   {action:'save', data:data, autosave:0},
	   function(data){
	       sb.attr('value', 'Saved');
	   }
	  )
}

function fade_out_inputs(){
    $('.editable').animate({
	backgroundColor: 'white',
	borderBottomColor: 'white',
	borderTopColor: 'white',
	borderLeftColor: 'white',
	borderRightColor: 'white'
    }, 800);
}



function fade_up_inputs(){
    $('.editable').animate({
	backgroundColor: input_background_color,
	borderBottomColor: input_border_color,
	borderTopColor: input_border_color,
	borderLeftColor: input_border_color,
	borderRightColor: input_border_color,
    }, 800, function(){clearTimeout(t); t = setTimeout('fade_out_inputs()', fade_timer)});
}