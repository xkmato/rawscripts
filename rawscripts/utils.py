from flask import Flask, request, render_template, g, make_response
from rawscripts import app
from rawscripts.db.models import db
import config
import random
from datetime import datetime
import json
from lxml.html.clean import Cleaner


@app.before_request
def get_config():
    a = { 'MODE': config.MODE,
          'GA': config.GA, 
        }
    g.t = a

@app.before_request
def request_is_mobile():
    m = mobileTest(request.user_agent.string)
    g.t['mobile']=m

def create_project(t):
    p = True
    while not p==None:
        alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        project_id=''
        for x in random.sample(alphabet,20):
            project_id+=x
        p = db.Projects.find_one({'project_id' : project_id})
    
    project = db.Projects()
    project.project_id = unicode(project_id)
    project.title = t
    project.users = [{'id':g.user.user_id, 'role':u'owner'}]
    project.resources = []
    project.save()
    return project_id


def create_resource(project_id, resource_type):
    project = db.Projects.find_one({'project_id' : project_id})

    #r = True #new resource_id to check for. assume it exists
    alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    resource_id=''
    for x in random.sample(alphabet,20):
        resource_id+=x

    
    # Create Screenplay entry in DB
    screenplay = {
        'type' : unicode(resource_type),
        'resource_id': unicode(resource_id),
        'title': u'',
        'created': datetime.utcnow(),
        'last_updated': datetime.utcnow(),
        'plugin_data':[],
        'users':[
            {'id':g.user.user_id,
             'role':u'owner',
             }
            ],
        }
    project.resources.append(screenplay)
    project.last_updated = datetime.utcnow()
    project.save()
    return resource_id
    



def permission(resource_id):
    item = db.Project.find_one({'resources.resource_id' : unicode(resource_id)})
    user = g.user.user_id
    return 'owner'
"""
    if user in item.users:
        return 'owner'
    elif user in item.viewers:
        return 'viewer'
    else:
        return False
"""

## html cleaner taken from mediagoblin. thanks cwebber!
# A super strict version of the lxml.html cleaner class
HTML_CLEANER = Cleaner(
    scripts=True,
    javascript=True,
    comments=True,
    style=True,
    links=True,
    page_structure=True,
    processing_instructions=True,
    embedded=True,
    frames=True,
    forms=True,
    annoying_tags=True,
    allow_tags=[
        'div', 'b', 'i', 'em', 'strong', 'p', 'ul', 'ol', 'li', 'a', 'br'],
    remove_unknown_tags=False, # can't be used with allow_tags
    safe_attrs_only=True,
    add_nofollow=True, # for now
    host_whitelist=(),
    whitelist_tags=set([]))

def clean_html(html):
    # clean_html barfs on an empty string
    if not html:
        return u''

    return HTML_CLEANER.clean_html(html)



import re
def mobileTest(s):
	patt = re.compile('android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|pixi|plucker|pocket|psp|symbian|treo|vodafone|wap|windows (ce|phone)|xda|xiino', flags=re.I)
	m = re.search(patt, s)
	if m:
		return 1
	else:
		patt = re.compile('1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw-(n|u)|c55/|capi|ccwa|cdm-|cell|chtm|cldc|cmd-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc-s|devi|dica|dmob|do(c|p)o|ds(12|-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(-|_)|g1 u|g560|gene|gf-5|g-mo|go(.w|od)|gr(ad|un)|haie|hcit|hd-(m|p|t)|hei-|hi(pt|ta)|hp( i|ip)|hs-c|ht(c(-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i-(20|go|ma)|i230|iac( |-|/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|klon|kpt |kwc-|kyo(c|k)|le(no|xi)|lg( g|/(k|l|u)|50|54|e-|e/|-[a-w])|libw|lynx|m1-w|m3ga|m50/|ma(te|ui|xo)|mc(01|21|ca)|m-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|-([1-8]|c))|phil|pire|pl(ay|uc)|pn-2|po(ck|rt|se)|prox|psio|pt-g|qa-a|qc(07|12|21|32|60|-[2-7]|i-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55/|sa(ge|ma|mm|ms|ny|va)|sc(01|h-|oo|p-)|sdk/|se(c(-|0|1)|47|mc|nd|ri)|sgh-|shar|sie(-|m)|sk-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h-|v-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl-|tdg-|tel(i|m)|tim-|t-mo|to(pl|sh)|ts(70|m-|m3|m5)|tx-9|up(.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(-|2|g)|yas-|your|zeto|zte-')
		s = s[0:4]
		m = re.search(patt, s)
		if m:
			return 1
		else:
			return 0
