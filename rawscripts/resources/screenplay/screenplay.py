# Rawscripts - Screenwriting Software
# Copyright (C) Ritchie Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, request, render_template, g, make_response
from rawscripts import app, connection
from rawscripts.db.models import db
from rawscripts.utils import create_resource
import json
from datetime import datetime


def get_template():
    with app.open_resource('resources/screenplay/screenplay.html') as f:
        template_string = f.read()
        return template_string

def get_template_values(project_id, resource_id, mode):
    
    dev_js = ['base', 'calc', 'ccp', 'dom-manipulate', 'draw', 'editor', 'init', 'keyboard', 'menu', 'mouse', 'notes', 'spellcheck', 'window']
    pro_js = ['editor-compiled']
    dev_css = ['menu', 'menuitem','menuseparator','common','toolbar','button','custombutton','autocomplete', 'dialog', 'linkbutton', 'tab', 'tabbar', 'colormenubutton', 'palette',	'colorpalette',	'editor/bubble', 'editor/dialog', 'editor/linkdialog', 'editortoolbar']
    pro_css = []
    g.t['EDITOR_CSS'] = pro_css if mode=="PRO" else dev_css
    g.t['EDITOR_JS'] = pro_js if mode=="PRO" else dev_js
    
    g.t['EOV'] = 'editor'
    g.t['user'] = g.user.nickname
    g.t['resource_id'] = resource_id
    g.t['project_id'] = project_id
    
    project = db.Projects.find_one({u'project_id':project_id})
    titlepage = 'none' #assume there is no titlepage for this screenplay
    for i in project.resources:
        if i['type'] == u'titlepage':
            titlepage=i['resource_id']
            break
    g.t['titlepage_resource_id']=titlepage
    return g.t

def get_current_save_data(project_id, resource_id):
    item = db.Projects.find_one({'resources.resource_id' : resource_id})
    data = db.ComponentData.find_one({u'resource_id' : resource_id}, sort = [(u'version', -1)])
    db_notes = db.Notes.find({u'resource_id':resource_id})
    threads = []
    for note in db_notes:
        notes = []
        for i in note.notes:
            if not i['is_trashed']:
                notes.append({
                        'note_id' : i['note_id'],
                        'user' : i['nickname'],
                        'content' : i['content'],
                        'unreadBool' : 1,
                        'timestamp' : str(i['timestamp'])
                        })
        if len(notes)>0 and note['is_trashed'] == False:
            threads.append({
                    'thread_id' : note['thread_id'],
                    'location' : note['location'],
                    'notes' : notes
                    })
            
    o = {'title': item.title,
         'lines': json.loads(data.data),
         'spelling': [],
         'notes': threads,
         'sharedwidth': [],
         'contacts': [],
         'autosave': 1
         }
    return json.dumps(o)

def process_save_data(data):
    return data

def create_new_resource(project_id):
    project = db.Projects.find_one({'project_id' : project_id})
    resource_id = create_resource(project_id, 'screenplay')
    # Create First Save
    d = db.ComponentData()
    d.resource_id = unicode(resource_id)
    d.version = 1
    d.autosave = False
    d.data = unicode(json.dumps([['Fade In:',1],['Int. ',0]]))
    d.save()
    
    return resource_id
