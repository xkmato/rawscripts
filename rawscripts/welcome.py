from flask import Flask, request, render_template, g
from rawscripts import app


# Provides a welcome page at the root web address. All it does is
# grabs the template and renders out analytics script
# 
# TODO dynamicly create openID urls and clear login buttons
@app.route('/')
def welcome():
    
    return render_template('welcome.html', t=g.t)
