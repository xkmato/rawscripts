from flask import Flask, request, render_template, g, make_response, session, redirect, url_for
from werkzeug import secure_filename
from rawscripts import app, connection, mail
from rawscripts.db.models import db
from flaskext.mail import Message
from rawscripts.utils import create_project, create_resource
import config
import convert

import StringIO
import json
from datetime import datetime

@app.route('/projects', methods=['GET', 'POST'])
def projects():
    if request.method=='GET':
        g.t['user'] = g.user.nickname

        if g.t['mobile']:
            return render_template('mobile.scriptlist.html', t=g.t)
        return render_template('scriptlist.html', t=g.t)

    # if POST is the method, figure out the intended 'action' and call
    # that function

        # figure out the intended action to be done
    r = request.values
    a = request.values['action']
    
    # action should be name of function. Call it
    response_text = projects_actions[a](r)

    # if no good error comes back, call unknown
    if not response_text:
        response_text = "Unknown Error"

    # Create http text/plain response
    response = make_response(json.dumps(response_text))
    response.headers["Content-type"] = "text/plain"
    return response


#
# Gets the complete list of projects and all thier details. Is used
# when getting formation for the scriptlist. Could also be exposed as
# and API
def projects_list(r):
    projects = db.Projects.find({'users.id' : g.user.user_id}, sort = [(u'last_updated', -1)])
    projects_out = []
    current_time = datetime.utcnow()
    for i in projects:
        
        resources = []
        for j in i['resources']:
            resources.append({
                    'type':j['type'],
                    'resource_id':j['resource_id'], 
                    'title':j['title'],
                    'last_updated': j['last_updated'].__str__()
                    }
                             )
        
        projects_out.append({
                'project_id':i.project_id,
                'title':i.title,
                'is_trashed':i.is_trashed,
                'resources':resources,
                'last_modified':i.last_updated.__str__(),
                })
    
    output = {
        'projects':projects_out,
        'folders':g.user.folders,
        }
    return output

# create a new project, loaded with screenplay resource
def create_new_project(r):
    project_id = create_project(r['title'])

    # create a resource to start off with. Bey default, this will be a
    # screenplay
    name = 'rawscripts.resources.screenplay.screenplay'
    resource_module = __import__(name, fromlist=['*'])
    reload(resource_module)
    resource_id = resource_module.create_new_resource(project_id)
    output = {
        'project_id':project_id,
        'resource_id':resource_id,
        'last_updated':datetime.utcnow().__str__(),
        }
    return output
    
# create a new folder
def create_folder(r):
    # get the id for the new folder. cycle through other ids, find
    # the next integer to use.
    new_id = 0
    for i in g.user.folders:
        if i['id']>new_id:
            new_id = i['id']
    new_id=new_id+1
                
    d = {
        u'id': new_id,
        u'name':unicode(r['name']),
        u'projects':[],
        }
        
    g.user.folders.append(d)
    g.user.save()

    return d

# delete a user defined folder
def delete_folder(r):
    folder_id = int(r['folder_id'])
    projects_in_folder = []

    # remove this folder from the users folder list
    for i in g.user.folders:
        if i['id']==folder_id:
            projects_in_folder = i['projects']
            g.user.folders.remove(i)
            g.user.save()
            break

    # create response
    return {u'folder_id':folder_id, u'projects_in_folder':projects_in_folder}


# remove a project from a user defined folder
def remove_project_from_folder(r):
    folder_id = int(r['folder_id'])

    for i in g.user.folders:
        if i['id']==folder_id:
            i['projects'].remove(r['project_id'])
            g.user.save()
            break

    d={
        u'folder_id':r['folder_id'],
        u'project_id':r['project_id'],
        }
    return d


# Add a project to a user defined folder
def add_project_to_folder(r):
    folder_id = int(r['id'])
    
    for i in g.user.folders:
        if i['id']==folder_id:
            i['projects'].append(r['project_id'])
            g.user.save()
            break
    
    d={'id':r['id']}
    return d

# move a project to trash
def move_project_to_trash(r):
    project_id = r['project_id']
    project = db.Projects.find_one({u'project_id' : project_id})

    project.is_trashed=True
    project.save()

    d={'project_id':project_id}
    return d

def remove_project_from_trash(r):
    project_id = unicode(r['project_id'])
    project = db.Projects.find_one({u'project_id': project_id})
    project.is_trashed = False
    project.save()

    d={'project_id':project_id}
    return d

def email_resource(r):
    msg = Message('From Rawscripts')
    msg.subject = r['subject'];
    msg.body = r['message']
    msg.recipients = [r['recipients[]']]
    msg.sender = 'no-reply@'+config.DOMAIN
    mail.send(msg)
    return {'response':'sent'}
#
# Registering all the posible notes actions
#
projects_actions = {
    'projects_list' : projects_list,
    'create_new_project': create_new_project,
    'create_folder': create_folder,
    'delete_folder': delete_folder,
    'add_project_to_folder': add_project_to_folder,
    'remove_project_from_folder':remove_project_from_folder,
    'move_project_to_trash': move_project_to_trash,
    'remove_project_from_trash': remove_project_from_trash,
    'email_resource': email_resource,
 }
