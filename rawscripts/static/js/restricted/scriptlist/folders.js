/**
 * Rawscripts - Screenwriting Software
 * Copyright (C) Ritchie Wilson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var folders = [];

/**
 * A Folder entity. stores all info for a folder
 */
function Folder(id, name, projects){
    this.id = id;
    this.name = name;
    this.projects = projects;
}

/**
 * Method on Folder, takes it's data and creates an HTML entry in the
 * scriptlist.
 */
Folder.prototype.createHTML = function(){
    
    // Create container div
    this.container = $('<div>');
    this.container.attr({
	'class':'folder'
    });
    
    // add folder image
    this.container.append(
	$('<img>').attr({
	    'src':'static/images/folder.png',
	    'class': 'folder-image',
	})
    );
    
    // create the text span in the folder
    this.container.append(
	$('<span>').html(this.name)
    );
    
    var folder = this;
    this.container.droppable({
	hoverClass: 'ui-state-hover',
	drop:function(event, ui){
	    var project_id = ui.draggable.attr('data-project_id');
 	    for(i in projects){
		if(projects[i].project_id==project_id){
		    var project = projects[i];
		}
	    }
	    project.add_project_to_folder(folder);
	}
    });
    
    this.container.click(function(){
	$('.folder-current').removeClass('folder-current');
	rs.folders.open_folder = folder.id;
	show_projects_in_folder();
    });
    
    this.container.hover(
	function(){
	    $(this).append(
		$('<span>')
		    .attr({'class':'ui-icon ui-icon-close'})
		    .css({'float':'right'})
		    .click(function(e){
			e.stopPropagation();
			folder.delete_folder();
		    })
	    )
	},
	function(){
	    $(this).find("span:last").remove();
	}
    )
    
    return this.container
}

/**
 * Method to delete this folder. Asks for confirmation, then makes the
 * ajax call and handles the response.
 */
Folder.prototype.delete_folder = function(){
    var c = confirm("you sure?");
    if (!c)return;
    
    var folder = this;
    $.post('/projects',
	   { action: 'delete_folder',
	     folder_id: folder.id,
	     name: folder.name,
	   },
	   function(data){
	       if(data.folder_id==undefined)return ERROR('Whoa, wrong data came back');
	       
	       var folder_id = parseInt(data.folder_id)
	       
	       
	       // Remove folder html in left column
	       folder.destroyHTML();
	       
	       // remove folder object
	       folders.splice(jQuery.inArray(folder, folders), 1);

	       for (i in projects){
		   if(jQuery.inArray(folder_id, projects[i].folders)>=0){
		       var pos = jQuery.inArray(folder_id, projects[i].folders); // position of this folder in array
		       projects[i].folders.splice(pos, 1);
		       
		       //redraw html
		       projects[i].create_folders_in_project();
		   }
	       }

       
	   },
	   'json'
	  
	  )
}

Folder.prototype.destroyHTML = function(){
    this.container.remove();
}

function new_folder_prompt(){
    // Create some html for the jquery dialog box
    var d = $('<div>');
    d.append($('<h2>').html('Create a New Folder'))
    new_folder_name_field = $('<input>').attr({
	'type':'text',
	'value':'Unitled Folder',
	'id':'new_folder_title'
    });
    d.append(new_folder_name_field);
    $('body').append(d);
    
    // Turn that html into the jquery dialog box
    d.dialog({
	modal:true,
	resizable:false,
	buttons:[{
	    text:'Create Folder',
	    click:function() {create_folder(d)}
	}],
	close:function(){d.remove()}
    });
}

function create_folder(d){
    // get the new folder name and check it to make sure it's valid
    var name = $('#new_folder_title').val();
    if(!check_new_title(name))return ERROR('Please give this folder a name')
    
    // update the gui in the dialog to show that things are working,
    // and user can't try to resubmit
    d.dialog({
	buttons:[{
	    disabled:true,
	    text:'Creating Screenplay...'
	}]
    });
    
    // post title and commands to the server
    $.post(
	'/projects',
	{name:name, action:'create_folder'},
	function(data){
	    // if we don't get the data we expect, throw an error and
	    // reset the GUI
	    if (data.id==undefined){
		d.dialog({
		    buttons:[{
			disabled:false,
			text:'Create Folder'
		    }]
		});
		return ERROR('Something went wrong');
	    }
	    
	    // if we do get the data we expect, create a new project object
	    var f = new Folder(data.id, data.name, []);
	    folders.push(f);
	    
	    // update html by creating new entry in the list and closing dialog
	    $('#user_folders').append(f.createHTML());
	    d.dialog('destroy');
	    d.remove();
	},
	'json'
    ).error(function(){
	d.dialog('destroy');
	d.remove();
	ERROR("Couldn't reach the server. Please check your internet connection and try again.");
    });
}