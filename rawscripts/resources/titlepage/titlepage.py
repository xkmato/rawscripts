# Rawscripts - Screenwriting Software
# Copyright (C) Ritchie Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, request, render_template, g, make_response
from rawscripts import app, connection
from rawscripts.db.models import db
from rawscripts.utils import clean_html, create_resource
import json
from datetime import datetime


def get_template():
    with app.open_resource('resources/titlepage/titlepage.html') as f:
        template_string = f.read()
        return template_string

def get_template_values(project_id, resource_id, mode):
    
    g.t['resource_id'] = resource_id
    g.t['project_id'] = project_id
    g.t['user']=g.user.nickname

    last_save = db.ComponentData.find_one({u'resource_id' : resource_id}, sort = [(u'version', -1)])
    g.t['data'] = json.loads(last_save.data)

    return g.t

def create_new_resource(project_id):
    # If there is no resource_id, create a new resource, then redirect
    # to the new, correct URL
    project = db.Projects.find_one({'project_id' : project_id})
    resource_id = create_resource(project_id, 'titlepage')
    first_save = db.ComponentData()
    first_save.resource_id = unicode(resource_id)
    first_save.version = 1
    first_save.autosave = True
    first_save.data = unicode(json.dumps({
                'title' : 'The Good The Bad and the Ugly',
                'written_by' : 'Written By<br><br>Arthur Sheekman<br>Harry Ruby<br>Bert Kalmar',
                'left_editable' : '183 E. 93rd St<br>Suite 9<br>NY, NY<br><br>212-555-5555',
                'right_editable' : 'WGA #10F6432GLAPO4<br>Copyright 2011'
                }
    ))
    first_save.save()
    return resource_id

def process_save_data(data):
    j = json.loads(data)
    for i in j.keys():
        clean = clean_html(j[i])
        clean = clean.replace('<p>','').replace('</p>','')
        j[i] = clean
    data = unicode(json.dumps(j))
    return data
