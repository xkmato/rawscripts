/**
 * Rawscripts - Screenwriting Software
 * Copyright (C) Ritchie Wilson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$(document).ready(function(){
    $.post('/projects',
	   {action:'projects_list'},
	   function(data){
               var x = data.projects;
               for(i in x){
		   var d = $('<div>').attr({'data-role':'collapsible'});
		   d.append($('<h3>').html(x[i]['title']));
		   d.append($('<p>').html('This is the text that ought to be shown on button press.'))
		   $('#list').append(d);
		   d.collapsible({refresh:true});
               }
	   },
	   'json'
	  )
})
