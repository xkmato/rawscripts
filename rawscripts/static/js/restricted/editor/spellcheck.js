/**
 * Rawscripts - Screenwriting Software
 * Copyright (C) Ritchie Wilson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * creating a spellcheck dialog object. Holds onto some useful
 * information, like where the spell check started, where it is
 * currently, and the jquery dialog box;
 */
function spellcheckDialog(){
    this.type = 'spellceck';
    
    this.start_row = 0;
    this.start_col = 0; // where we started checking from
    
    this.row = 0;
    this.col = 0; // where we are currently checking

    // create HTML
    var d = $('<div>');
    $('body').append(d);
    // a neeto text box
    d.append(
	$('<div>').attr({
	    'class':'textbox',
	    'contenteditable':'true',
	    'id':'sSentance'
	})
    )
    d.append(
	$('<div>').attr({
	    'id':'sSuggest'
	})
    )
    // Create the buttons to control the spell checker
    var spellControls = $('<div>').attr({'id':'spellControls'});
    var button_data = ['Change', 'Ignore', 'Ignore All'];
    for (i in button_data){
	spellControls.append(
	    $('<button>')
		.html(button_data[i])
		.attr({'class':'spellcheck_button'})
		.button()
	)
    }
    d.append(spellControls)
    
    // add some on click methods to the buttons
    
    this.change_button = $(".spellcheck_button:contains('Change')");
    this.change_button.click(function(){rs.open_dialog.change_misspelled_word()});

    this.ignore_button = $(".spellcheck_button:contains('Ignore')");
    this.ignore_button.click(function(){rs.open_dialog.find_next_wrong_word()})

    this.ignore_all_button = $(".spellcheck_button:contains('Ignore All')");
    this.ignore_all_button.click(function(){rs.open_dialog.ignore_all()})

    d.dialog({
	modal:true,
	width:575,
	title:'Spell Check',
	close:function(){d.remove()}
    });
    
    this.dialog = d;
    
}


function spellcheckSuggestion(suggestion){
    this.suggestion = suggestion;
    this.container = $('<div>')
	.addClass('spellcheckitem')
	.text(spellWrong[word].suggestions[i]);
    var suggestion = this;
    this.container.click(function(){
	$('#wrong_word').html(suggestion.suggestion);
	$('.spellcheckitem-selected').removeClass('spellcheckitem-selected');
	$(this).addClass('spellcheckitem-selected');
    })
    
    return this.container;
}

/**
 * Launches the spell check window, and handles little necessary
 * things first.
 */
function launchSpellCheck(){
    if(EOV=='viewer')return;
    
    // take focus off canvas, spell check current line
    typeToScript=false;
    ajaxSpell(pos.row)
    
    // add ignored words back into wrong words list
    var obj;
    while(obj = spellIgnore.pop()){
	spellWrong.push(obj);
    }
    
    // create dialog objects, and attach it to the global Rawscripts
    // object
    var d = new spellcheckDialog();
    rs.open_dialog = d;

    rs.open_dialog.find_next_wrong_word();
    
}

/**
 * Finds the next incorrect word in the text. Then uses that info to
 * construct the apropriate GUI including highligted word and
 * suggestions.
 */
spellcheckDialog.prototype.find_next_wrong_word = function (){
    if(EOV=='viewer')return;
    // starting at this objects current row, go through lines finding
    // one that has at least one mistake
    for (var line = this.row; line<lines.length; line++){
	for(word in spellWrong){
	    var re = new RegExp('\\b'+spellWrong[word].word+'\\b', 'g');
	    re.lastIndex = this.col;
	    var results = re.exec(lines[line].text)
	    if (results!=null){
		// we know that there is a spelling mistake in this
		// line. Keep going through spellWrong to make sure
		// this is the NEXT misspelled word. Not just one of
		// many in the line.
		var next_word=word;
		next_word++;
		for (next_word; next_word<spellWrong.length;next_word++){
		    var new_re = new RegExp('\\b'+spellWrong[next_word].word+'\\b', 'g');
		    new_re.lastIndex = this.col;
		    var new_results = new_re.exec(lines[line].text);
		    if(new_results!=null && new_results<results){
			re = new_re;
			results = new_results;
		    }
		}
		
		// create html to go in sentance box. Insert it
		var inserted_html = lines[line].text.substring(0,results.index);
		inserted_html=inserted_html+'<span id="wrong_word" style="color:red">'+results+"</span>";
		inserted_html=inserted_html+lines[line].text.substring(re.lastIndex);
		$('#sSentance').html(inserted_html);
		
		// Clear, then repopulate the suggestion list
		$('#sSuggest').html('')
		for (i in spellWrong[word].suggestions){
		    var suggest = new spellcheckSuggestion(spellWrong[word].suggestions[i]);
		    $('#sSuggest').append(suggest);
		}
		this.row = line;
		this.col = re.lastIndex;
		this.wrong_word = results;
		return;
	    }
	}
	// if we've looped through this line and havn't found
	// anything, move searching column to 0
	this.col = 0;
    }
}

/**
 * method on the spellcheck "Ignore All" button. It takes this word
 * object out of the spellWrong list, and puts it in the spellIgnore
 * list. That ignore list is added back to spellWrong when spellcheck
 * is launched next time.
 */
spellcheckDialog.prototype.ignore_all = function(){
    // take out the word from spellWrong list, and add it to the
    // spellIgnore list
    for(i in spellWrong){
	if(spellWrong[i].word==this.wrong_word){
	    spellIgnore.push(spellWrong[i]);
	    spellWrong.splice(i,1);
	}
    }
    rs.open_dialog.find_next_wrong_word();
}


/**
 * Changes the one word to one of the suggestions.
 *
 * More specifically, a user has the option to manually change the
 * text in the spellcheck window. Whatever is in that box will be
 * entered as the new text for that row.
 */
spellcheckDialog.prototype.change_misspelled_word = function(){
    // get new text and put it in its place
    var txt = $('#sSentance').text()
    lines[this.row].text=txt;
    
    // recalc things on this line
    getLines(this.row);
    pagination();
    
    // go to next word
    rs.open_dialog.find_next_wrong_word();
}

/**
 * Called on load to spell check the whole thing. Adds every line to
 * an array, and send that array to ajaxSpell
 */
function spellcheck_whole_screenplay(){
    var arr=[];
    var i=lines.length-1;
    while(i>=0){
	arr.push(i);
	i--;
    }
    ajaxSpell(arr);
}


/**
 * ajaxSpell is given 'v', an array of numbers, which are lines whos
 * spelling need be checked. Going about ten pages at a time, this
 * sends all the text to the server, get spelling corrections
 * back. For lots of text, this is split up into about ten page
 * chunks, and the next ten pages arn't sent until that last bit gets
 * sorted.
 */
function ajaxSpell(v){
    if(EOV=='viewer')return;
    checkSpell=false;
    var lines_to_check = '';
    if (typeof v == 'number')lines_to_check = lines[v].text;
    else{
	// v is an array of lines to check. concat them all to send at
	// the same time to be spell checked
	while(v.length>0){
	    var line = lines[v.pop()];
	    lines_to_check=lines_to_check+line.text+' ';
	    if(lines_to_check.length>10000)break;
	}
    }
    $.post('/spellcheck',
	   {resource_id:resource_id, lines_to_check:lines_to_check},
	   function(data){
	       // add returned data to the spellWrong list, checking
	       // to avoid duplicates
	       for (new_word in data){
		   var exists = false;
		   for (existing_word in spellWrong){
		       if (data[new_word].word==spellWrong[existing_word].word){
			   exists = true;
			   break;
		       }
		   }
		   // if this word isn't in spell check array, add it
		   if(exists==false){
		       spellWrong.push(data[new_word]);
		   }
	       }
	       
	       // if there are still more lines to check, call this
	       // function again with list of remaining lines to check
	       if(v.length>0){
		   ajaxSpell(v,0);
	       }
	   },
	   'json'
	  )
}
