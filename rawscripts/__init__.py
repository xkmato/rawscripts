from flask import Flask
app = Flask(__name__)
from flask import Flask
from mongokit import Connection, Document
from flaskext.mail import Mail

# configuration
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
TESTING = True

# create the little application object
app = Flask(__name__)
app.config.from_object(__name__)

# connect to the database
connection = Connection(app.config['MONGODB_HOST'],
                        app.config['MONGODB_PORT'])

# set up mailserver if'n you want
mail = Mail(app)

import rawscripts.welcome
import rawscripts.scriptlist
import rawscripts.editor
import rawscripts.utils
import rawscripts.db.models
import rawscripts.auth
import rawscripts.notes
import rawscripts.export
import rawscripts.upload
import rawscripts.spellcheck

# set the secret key.  keep this really secret:
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
