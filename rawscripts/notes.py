# Rawscripts - Screenwriting Software
# Copyright (C) Ritchie Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, request, render_template, g, make_response
from rawscripts import app
from rawscripts.db.models import db
from rawscripts.utils import permission
from rawscripts.utils import clean_html
import config
import json
import random
import datetime


#
# All notes actions are posted through this route. Then, depending on
# the action variable, the message is sent through the proper
# function.
#
@app.route('/notes', methods=['POST'])
def notes():
    # figure out the intended action to be done
    r = request.values
    a = request.values['action']
    
    # action should be name of function. Call it
    response_text = notes_actions[a](r)

    # if no good error comes back, call unknown
    if not response_text:
        response_text = "Unknown Error"

    # Create http text/plain response
    response = make_response(response_text)
    response.headers["Content-type"] = "text/plain"
    return response

#
# This is called for any new note to be added. That includes the first
# note of a thread when the thread needs to be created. That includes
# when a past note is being edited an a new one put in its place.
#
def new_note(r):
    r_id = r['resource_id']
    p = permission(r_id)
    if p == 'owner':
        # thread_id is 'new' when this is the first message in the
        # thread. Create the thread before moving on.
        if r['thread_id']=='new':
            thread_id = new_thread(r)
        else:
            thread_id = int(r['thread_id'])

        # thread is the db object we're adding to
        thread = db.Notes.find_one({u'thread_id':thread_id})
        
        # create new note object to insert
        note_id = thread.list_length_cache + 1
        note = {
            'note_id' : note_id,
            'user' : g.user.openid,
            'nickname' : g.user.nickname,
            'content' : clean_html(r['content']),
            'is_trashed' : False,
            'unready_by' : [],
            'timestamp': datetime.datetime.utcnow(),
            'replaced_by' : 0,
            }
        
        # reconstruct the note list, putting the new note in the
        # correct place. that is someone in the middle if it replaces
        # another. At the end if it is new.
        new_note_inserted = False
        new_note_list = []
        for i in thread.notes:
            if r['msg_id'][:3]!='new' and i['note_id'] == int(r['msg_id']):
                i['is_trashed'] = True
                i['replaced_by'] = note_id
                new_note_list.append(i)
                new_note_list.append(note)
                new_note_inserted=True
            else:
                new_note_list.append(i)
        
        # Note wasn't put in the middle, so it should be placed at the
        # end of the list
        if new_note_inserted == False:
            new_note_list.append(note)
        thread.notes = new_note_list
        
        # update list length cache
        thread.list_length_cache = note_id
        
        # save the work that's been done
        thread.save()
        
        # return text of exactly what was entered into the db
        return json.dumps({'thread_id': thread_id, 'note_id':note_id, 'user':g.user.nickname, 'content':clean_html(r['content'])})
    else:
        return "Permission Error"


#
# another notes "action". Trashes, but does not delete, a single
# message within a thread.
#
def trash_message(r):
    r_id = r['resource_id']
    p = permission(r_id)
    if p == 'owner':
        # thread is the db object which hold the message to be trashed
        thread = db.Notes.find_one({'thread_id':int(r['thread_id'])})
        
        # find correct message from list of messages
        for i in thread.notes:
            if i['note_id'] == int(r['note_id']):
                i['is_trashed']=True
                break
            
        # save the altered entry
        thread.save()
        return 'trashed'
    else:
        return "Permission Error"
#
# Another notes "action". Trashes, but does not delete, an entire
# thread full of messages.
#
def trash_thread(r):
    r_id = r['resource_id']
    p = permission(r_id)
    if p == 'owner':
        thread = db.Notes.find_one({'thread_id':int(r['thread_id'])})
        thread.is_trashed = True
        thread.save()
        return "trashed"
    else:
        return "Permission Error"

#
# Update the positions of notes in a screenplay as necessary
#
def notes_locations(r):
    r_id = r['resource_id']
    p = permission(r_id)
    if p:
        
        threads = db.Notes.find({u'resource_id' : r_id})
        new_pos = json.loads(r['locations'])
        for i in new_pos:
            for j in threads:
                if i['thread_id'] == j.thread_id:
                    j.location = {u'row': i['row'], u'col':i['col']}
                    j.save()
                    break
        return 'updated'
    else:
        return 'Permission Error'

# Utility to create the structure of a Note. this is an empty thread ready for
# messages to be added.
def new_thread(r):
    last = db.Notes.find_one({}, sort = [(u'thread_id', -1)])
    if last==None:
        thread_id = 1
    else:
        thread_id = last.thread_id + 1 

    note = db.Notes()
    note.resource_id = r['resource_id']
    note.thread_id = thread_id
    note.location = json.loads(r['location'])
    note.notes = []
    note.list_length_cache = 0
    note.is_trashed = False
    note.save()
    return thread_id


#
# Registering all the posible notes actions
#
notes_actions = {
    'new': new_note,
    'trash_thread': trash_thread,
    'trash_message' : trash_message,
    'locations' : notes_locations,
}
