/**
 * Rawscripts - Screenwriting Software
 * Copyright (C) Ritchie Wilson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * A Project entity. Sores all the relevant infor for a project in one
 * accessable object. This creates its starting attributes.
 */
function Project(project_id, resource_id, title, updated){
    this.project_id = project_id;
    this.title = title;
    this.last_updated = updated;
    this.resource_id = resource_id;
    this.folders = [];
}

/**
 * Create a Rename prompt for a project
 */
Project.prototype.rename_prompt = function(){
    var d = $(document.createElement('div'));
    this.dialog_html = d;
    d.append($(document.createElement('h2')).html('Rename This Project'))
    this.rename_field = $(document.createElement('input')).attr({
	'type':'text',
	'value':this.title
    });
    d.append(this.rename_field);
    $('body').append(d);
    var project = this;
    
    d.dialog({
	modal:true,
	resizable:false,
	title:"Rename "+project.title,
	buttons:[{
		text:'Rename',
		click:function() {project.rename_project(d)}
	}],
	close:function(){d.remove()}
    });
}


/**
 * Called from a Rename prompt, does the actual AJAX work of changing
 * the name of a project, then updates the scriptlist UI
 * param {object} dialog -- the jquery dialog box for 'rename'
 */
Project.prototype.rename_project = function(dialog){
    if (new_title == ''){return;}
    var project_id = this.project_id;
    var resource_id = this.resource_id;
    var new_title = this.rename_field.val();
    var project = this;
    $.post('/editor/'+project_id+'/'+resource_id,
	   {action:'rename', new_title:new_title},
	   function(data){
	       // data is either the server's interpreation of the new
	       // title (html cleaned) or it is an error message
	       project.title = data;
	       dialog.dialog('destroy');
	       project.dialog_html.remove();
	       
	       // avilible hook for after renameing project
	       try{rename_project_return(project)}
	       catch(err){};
	   }
	  ).error(function(){
	      dialog.dialog('destroy');
	      project.dialog_html.remove();
	      ERROR("Couldn't reach the server. Please check your internet connection and try again.");
	  });
}

/**
 * Opens a dialog box asking for a title for a new project
 */
function new_project_prompt(){
    // hook to allow pages to do some work before opening the prompt
    try{before_new_project_prompt()}
    catch(err){};
    
    // Create some html for the jquery dialog box
    var d = $(document.createElement('div'));
    d.append($(document.createElement('h2')).html('Create a New Screenplay'))
    new_project_title_field = $(document.createElement('input')).attr({
	'type':'text',
	'value':'Unitled Screenplay',
	'id':'new_project_title'
    });
    d.append(new_project_title_field);
    $('body').append(d);
    
    // Turn that html into the jquery dialog box
    d.dialog({
	modal:true,
	resizable:false,
	buttons:[{
	    text:'Create Screenplay',
	    click:function() {create_project(d)}
	}],
	close:function(){d.remove()}
    });
}


/**
 * Sends the user created screenplay title to the server. Server
 * creates new screenplay, responds with new, unique resource id.
 */
function create_project (d){
    // get the new title and check it to make sure it's valid
    var title = $('#new_project_title').val();
    if(!check_new_title(title))return ERROR('Please give this project a title')

    // update the gui in the dialog to show that things are working,
    // and user can't try to resubmit
    d.dialog({
	buttons:[{
	    disabled:true,
	    text:'Creating Screenplay...'
	}]
    });
    
    // post title and commands to the server
    $.post(
	'/projects',
	{title:title, action:'create_new_project'},
	function(data){
	    // if we don't get the data we expect, throw an error and
	    // reset the GUI
	    if (data.project_id==undefined){
		d.dialog({
		    buttons:[{
			disabled:false,
			text:'Create Screenplay'
		    }]
		});
		return ERROR('Something went wrong');
	    }

	    d.dialog('destroy');
	    d.remove();
	    
	    // availible hook to call after creating new project
	    try{after_create_new_project();}
	    catch(err){};
	    
	    // open the screenplay in a new window
	    window.open('/editor/'+data.project_id+'/'+data.resource_id);
	},
	'json'
    ).error(function(){
	d.dialog('destroy');
	d.remove();
	ERROR("Couldn't reach the server. Please check your internet connection and try again.");
    });
}





/**
 * Opens email prompt GUI on click
 * @ params { string } v resource_id of script
 */
Project.prototype.email_prompt = function(){
    try{before_email_prompt()}
    catch(err){};
    
    var d = $(document.createElement('div'));
    this.dialog_html = d;

    d.append($('<br>'));
    d.append($('<b>').html('Recipients'));
    d.append($('<br>'));
    
    this.recipients_field = $('<textarea>').attr({
	'rows':4,
	'class': 'email_text_input',
    });
    d.append(this.recipients_field);

    d.append($('<br>'));
    d.append($('<br>'));
    d.append($('<b>').html('Subject'))
    d.append($('<br>'));

    this.subject_field = $('<input>').attr({
	'type':'text',
	'class':'email_text_input',
	'value': this.title
    });
    d.append(this.subject_field);
    
    d.append($('<br>'));
    d.append($('<br>'));
    d.append($('<b>').html('Optional Message'));
    d.append($('<br>'));

    this.message_field = $('<textarea>').attr({
	'rows':4,
	'class': 'email_text_input'
    });
    d.append(this.message_field);
    
    
    $('body').append(d);
    var project = this;
    
    d.dialog({
	modal:true,
	resizable:false,
	width:'430px',
	title:'Email "'+project.title+'"',
	buttons:[{
		text:'Email Screenplay',
		click:function() {project.email_resource(d)}
	}],
	close:function(){d.remove()}
    });
}

Project.prototype.email_resource = function(d){
    // get list of recipients from string
    var bits = this.recipients_field.val().split(/[\s,]+/);
    var recipients = [];
    for (i in bits){
	if(isValidEmailAddress(bits[i])){
	    recipients.push(bits[i]);
	}
	else{
	    return ERROR(bits[i]+' is not a valid email address.')
	}
    }
    if(recipients.length==0)return ERROR('Please add some valid email addresses');
    if(recipients.length>5) return ERROR('You can email to just 5 people Maximum.')
    
    var subject = this.subject_field.val();
    var message = this.message_field.val();

    $.post('/projects',
	   {
	       action:'email_resource',
	       project_id: this.project_id,
	       resource_id: this.resource_id,
	       subject: subject,
	       message: message,
	       recipients: recipients
	   },
	   function(data){
	       d.remove();
	       if(typeof after_email_resource == 'function')after_email_resource();
	       alert("Sent! But not really...")
	   },
	   'json'
	  )
}