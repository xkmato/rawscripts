#from mongokit import Document
from flaskext.mongokit import MongoKit, Document
from datetime import datetime
from rawscripts import app

class Users(Document):
	"""
	All the data on a user. Adding in extra fields for when the
	RawScripts is awesomer.

	Structure
	
	- openid: the openid identifier for user. Main, unique identifier
	- email: email address of this user
	- nickname: optional nickname for user
	- first_login: date of the first time user logged in
	- last_login: date of most recent login
	- is_active: bool, is user active (so don't have to delete)
	- is_admin: bool, whether or not user is admin

	"""
	__collection__ = 'users'
	structure = {
		'user_id':int,
		'openid': unicode,
		'email': unicode,
		'nickname': unicode,
		'plugin_data':[dict], #nothing much going on there now
		'first_login': datetime,
		'last_login': datetime,
		'is_active': bool,
		'is_admin': bool,
		'folders':[{'id':int,
			    'name':unicode,
			    'projects':list,
			    }],
		}

	required_fields = ['openid']
	default_values = {
		'first_login': datetime.utcnow(),
		'last_login': datetime.utcnow(),
		'is_active':True, 
		'is_admin':False,
		'folders':[],
		}
	use_dot_notation = True

class Projects (Document):
	"""
	List of Projects in Database
	
	A Project will be a collection of data related to a single
	multimedia project. Currently, the 'components of' a project
	is just a screenplay. In the future, it could be multiple
	screenplays, or a add a calendar, or extra notes, or
	production sheets, or storyboards, or anything.
	
	
	Structure

	- project_id: unique id for this project
	- title: user generated title for the whole project
	- created: when it was created
	- last_updated: when a resource was last modified
	- folders: list of folders this project is in
	- plugin_data: yeah
	
	- users: list of users with some access to the project
	- users.id: unique user identifier
	- users.role: name of role for user, which determines level of access
	- users.folders: folders that a user has put this project in
	-
	- resources: list of resources attached to this project
	- resources.type: What kind of project component is this? Screenplay,
	  Calendar, Storyboard, Title Page, etc
	  
	- resources.resource_id: unique id for the script 
	- resources.title: User provided title 
	- resources.last_updated: kinda caching most recent timestamp  
	- resources.plugin_data: list of plugins. Placeholder for now.
	- resources.users: list of users and their roles in this resource
	- resources.users.id: user id
	- resources.users.role: role, determining permisssions
	--
	"""
	__collection__ = 'projects'
	structure = {
		'project_id': unicode,
		'title': unicode,
		'created': datetime,
		'last_updated': datetime,
		'is_trashed': bool,
		'plugin_data':[dict],
		'users' :[
			{'id':int,
			 'role':unicode,
			 }
			],
		'resources': [
			{'type' : unicode,
			 'resource_id': unicode,
			 'title': unicode,
			 'created': datetime,
			 'last_updated': datetime,
			 'plugin_data':[dict],
			 'users':[
					{'id':int,
					 'role':unicode,
					 }
					],
			 }
			],
		}

	default_values = {
		'created' : datetime.utcnow(),
		'last_updated' : datetime.utcnow(),
		'is_trashed': False,
		}
	use_dot_notation = True

class ComponentData (Document):
	"""
	The actual data stored for every save of a Project Component
	
	- resource_id : the id of the project component this is for
	
	- version : integer counting up on each save. First save is
          version 1, second save v 2, etc
	- autosave : boolean, if this was saved automatically or not
	
	- tags : list of ways this draft is tagged. Could be user
          generated {'manual' : 'Second Draft'}, or autmatically
          generated like {'export':'pdf'}
	- timestamp : you know. timestamp
	- data : the actual data
	- plugin_data : placeholder for now
	"""
	__collection__ = 'componentdata'
	structure = {
		'resource_id': unicode,
		'version' : int,
		'autosave' : bool,
		'tags' : [dict],
		'timestamp' : datetime,
		'data' : unicode,
		'plugin_data' : [dict],
		}
	default_values = {
		'timestamp' : datetime.utcnow(),
		'tags' : [],
		'plugin_data' : [],
		}
	use_dot_notation = True


class Notes (Document):
	"""
	Storing user generated notes for any particual ProjectComponent
	
	- resource_id : the resource that this note is a part of
	- thread_id : the unique id of this thread
	- location : list of coordinates for where the note is. On a
          screenplay it might be something like [row:3, col:10]. On a
          storyboard it might be [page:2, slide:1], or any other
          convention
	- notes : list of actual notes (non-threaded conversations)
	- notes.note_id : unique id for this note
	- note.user : user identifier for this note
	- note.nickname : user nickname cached with note
	- note.content : actual content in html
	- note.is_trashed : if note was trashed by user, thus isn't shown
	- note.unread_by : list of people who still get notified about this note
	- note.timestamp : timestamp
	- note.replaced_by : if note is edited, the old note
          is_trashed, then replaced_by is the note_id of the note
          which replaced it.
	- list_length_cache : keeps runing total of notes list to add
          proper iterative ids
	- is_trashed : if the whole thread is deleted
	- plugin_data : yeah
	"""
	__collection__ = "notes"
	structure = {
		'resource_id' : unicode,
		'thread_id' : int,
		'location' : dict,
		'notes' : [
			{'note_id' : int,
			 'user' : unicode,
			 'nickname' : unicode,
			 'content' : unicode,
			 'is_trashed' : bool,
			 'unread_by' : [unicode],
			 'timestamp' : datetime,
			 'replaced_by' : int,
				}
			],
		'list_length_cache' : int,
		'is_trashed' : bool,
		'plugin_data' : [dict],
		}
	default_values = {
		'plugin_data' : [],
		}
	use_dot_notation = True


"""
class SpellingData (db.Model):
	resource_id = db.StringProperty()
	wrong = db.TextProperty()
	ignore = db.TextProperty()
	timestamp = db.DateTimeProperty(auto_now_add=True)

class ShareNotify (db.Model):
	user= db.StringProperty()
	resource_id = db.StringProperty()
	timeshared = db.DateTimeProperty()
	timeopened = db.DateTimeProperty()
	opened = db.BooleanProperty()

	
class NotesNotify (db.Model):
	resource_id = db.StringProperty()
	thread_id = db.StringProperty()
	user = db.StringProperty()
	new_notes= db.IntegerProperty()

class UnreadNotes (db.Model):
	resource_id = db.StringProperty()
	thread_id = db.StringProperty()
	user = db.StringProperty()
	msg_id = db.StringProperty()
	timestamp = db.DateTimeProperty(auto_now_add=True)


class TitlePageData (db.Model):
	resource_id = db.StringProperty()
	title = db.StringProperty()
	authorOne = db.StringProperty()
	authorTwo = db.StringProperty()
	authorTwoChecked = db.StringProperty()
	authorThree  = db.StringProperty()
	authorThreeChecked  = db.StringProperty()
	based_on  = db.StringProperty()
	based_onChecked  = db.StringProperty()
	address = db.StringProperty()
	addressChecked = db.StringProperty()
	phone = db.StringProperty()
	phoneChecked = db.StringProperty()
	cell = db.StringProperty()
	cellChecked = db.StringProperty()
	email = db.StringProperty()
	emailChecked = db.StringProperty()
	registered = db.StringProperty()
	registeredChecked = db.StringProperty()
	other = db.StringProperty()
	otherChecked = db.StringProperty()


class DuplicateScripts (db.Model):
	new_script = db.StringProperty()
	from_script = db.StringProperty()
	from_version = db.IntegerProperty()

class Folders (db.Model):
	data = db.StringProperty()
	user = db.StringProperty()

class UsersSettings(db.Model):
	autosave = db.BooleanProperty()
	owned_notify = db.StringProperty()
	shared_notify = db.StringProperty()

class YahooOAuthTokens (db.Model):
	t = db.TextProperty()

class ActivityDB (db.Model):
	activity = db.StringProperty()
	user = db.StringProperty()
	resource_id = db.StringProperty()
	timestamp = db.DateTimeProperty(auto_now_add=True)
	mobile = db.IntegerProperty()
	size = db.IntegerProperty()
	new_notes = db.IntegerProperty()
	autosave = db.IntegerProperty()
	thread_id = db.StringProperty()
	numberOfScripts = db.IntegerProperty()
	scriptName = db.StringProperty()
	format = db.StringProperty()
	numberOfRecipients = db.IntegerProperty()
	fromPage = db.StringProperty()
	error = db.StringProperty()
"""

#connection.register([Users])
db = MongoKit(app)
db.register([Users, 
	     Projects,
	     ComponentData,
	     Notes,
	     ])
