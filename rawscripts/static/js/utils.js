/**
 * Rawscripts - Screenwriting Software
 * Copyright (C) Ritchie Wilson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



// Javascript Email Validator, yanked from 
// http://stackoverflow.com/questions/2381840/validate-form-with-javascript-using-onsubmit
// got to find a better, maintained solution
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

/**
 * this should check a user input for new project. Make sure it looks
 * kinda alright before moving on.
 * param {string} title title to check
 */
function check_new_title(title){
    if(title=='') return false;
    return true;
}

/**
 * When an error is thrown, open up the error box at the top center of
 * the browser window
 *
 * param {string} msg The error message to explain
 */
function ERROR(msg){
    $('#ERROR').show('blind', function() {
	setTimeout(function() {
	    $( "#ERROR" ).fadeOut();
	}, 8000 );
    });
    $('#error_msg').html(msg)
}