# Rawscripts - Screenwriting Software
# Copyright (C) Ritchie Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, request, g, make_response, render_template_string
from rawscripts import app, connection
from rawscripts.db.models import db
from rawscripts.utils import permission
import config
import json
from datetime import datetime
from enchant.checker import SpellChecker

@app.route('/spellcheck', methods=['POST'])
def spell():
	r = request.values
	resource_id = r['resource_id']
	data = r['lines_to_check']
	
	chkr = SpellChecker('en_US')
	wrong_words = []
	chkr.set_text(data)
	for err in chkr:
		d = {'word':err.word,
		     'suggestions':err.suggest(),
		     }
		wrong_words.append(d)

	response = make_response(json.dumps(wrong_words))
	response.headers["Content-type"] = "text/plain"
	return response
